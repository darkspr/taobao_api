# taobao_api
淘宝小型API

使用python+selenium+chrome 保持登陆淘宝
封装各种常规操作成接口，提供自动化处理订单

提供如下接口
- 订单获取接口: trades_sold_get
- 订单详情接口: trade.get
- 订单发货接口: logistics_online_send
- 无需物流发货: logistics_dummy_send
- 增加备注接口: trade_memo_add
- 修改备注接口: trade_memo_update



## 文件说明：

taobao_login.py:登录淘宝模块

- 千牛登录模式：通过千牛，火狐可以读取到登录状态直接登录（可靠性高，当前使用模式）

- 常规登录模式：登录使用二维码登录，发送邮件到指定邮箱，手机上绑定邮件，即可实现手机扫码（可靠性高）#已废弃

- 自动登录模式：通过登录支付宝登录淘宝，目前不需要滑动验证，有验证码可以对接打码平台（可靠性低，还在测试中）#已废弃



code_str.py:由于网易邮箱发送带图片的邮件显示垃圾邮件，不给发，所以把二维码转化成字符


taobao_trade.py：订单采集更新模块

- 自动采集新订单信息

- 自动更新有变化的订单信息

- 数据保存入TB.db数据库


taobao_api.py:提供功能接口



taobao_run.py:主程序模块，保持脚本稳定运行，处理api接口请求任务







